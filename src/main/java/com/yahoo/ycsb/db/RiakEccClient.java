/*
 * Contribution to YCSB by Viet Bui (from @uclouvain.be) 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yahoo.ycsb.db;

import com.yahoo.ycsb.DB;
import com.yahoo.ycsb.DBException;
import com.yahoo.ycsb.Status;

import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.ExecutionException;


import com.yahoo.ycsb.ByteIterator;
import com.yahoo.ycsb.DB;
import com.yahoo.ycsb.DBException;

import com.ericsson.otp.erlang.*;

import static com.yahoo.ycsb.db.RiakUtils.*;

/**
 * @author Basho Technologies, Inc.
 */
public final class RiakEccClient extends DB {

	private boolean _debug = false;

    public static final String DEFAULT_ERL_NODE = "erlNode@localhost";
    public static final String DEFAULT_ERL_COOKIE = "ecc-ycsb";
    public static final String DEFAULT_JAVA_CLIENT = "javaNode";
    public static final String DEFAULT_ERL_MODULE = "ecc_java";

    private String peer = DEFAULT_ERL_NODE;
    private String cookie = DEFAULT_ERL_COOKIE;
    private String javaClient = DEFAULT_JAVA_CLIENT;
    private String java_erl = DEFAULT_ERL_MODULE;

	private static OtpConnection conn;
    public OtpErlangObject received;

	public void init() throws DBException {
		loadProperties();
		//System.out.print("Please wait, connecting to "+peer+"....\n");

		try {
         OtpSelf self = new OtpSelf(javaClient, cookie.trim());
         OtpPeer other = new OtpPeer(peer.trim());
         conn = self.connect(other);
         //System.out.println("Connection Established with "+peer+"\n");
       }
       catch (Exception exp) {
         System.out.println("Connection error:" + exp.toString());
         exp.printStackTrace();
       }
	}
	
	/**
	 * Read a record from the database. Each field/value pair from the result will be stored in a HashMap.
	 *
	 * @param table The name of the table (Riak bucket)
	 * @param key The record key of the record to read.
	 * @param fields The list of fields to read, or null for all of them
	 * @param result A HashMap of field/value pairs for the result
	 * @return Zero on success, a non-zero error code on error
	 */
	@Override
	public Status read(String table, String key, Set<String> fields, HashMap<String, ByteIterator> result) {


		try {
              conn.sendRPC(java_erl,"lookup", new OtpErlangObject[] {new OtpErlangAtom(key)});
              OtpErlangObject response = conn.receiveRPC(); 
              OtpErlangTuple tuple = (OtpErlangTuple) response;
              if (tuple.arity() == 2){ //meaning not found
                 return Status.ERROR;
              }
              return Status.OK;
        }
        catch (Exception exp) {
               System.out.println("Error is :" + exp.toString());
               exp.printStackTrace();
               return Status.ERROR;
        }
	}
	
	
	/**
	 * Perform a range scan for a set of records in the database. Each field/value pair from the result will be stored in a HashMap.
	 *
	 * Note: The scan operation requires the use of secondary indexes (2i) and LevelDB. 
	 *
	 * @param table The name of the table (Riak bucket)
	 * @param startkey The record key of the first record to read.
	 * @param recordcount The number of records to read
	 * @param fields The list of fields to read, or null for all of them
	 * @param result A Vector of HashMaps, where each HashMap is a set field/value pairs for one record
	 * @return Zero on success, a non-zero error code on error
	 */
	@Override
	public Status scan(String table, String startkey, int recordcount, Set<String> fields, Vector<HashMap<String, ByteIterator>> result) {
		System.err.println("Do not need it yet. Sorry, just dumb it here.");
		return Status.ERROR;
	}
	

	/**
	 * Insert a record in the database. Any field/value pairs in the specified values HashMap 
	 * will be written into the record with the specified record key. Also creates a
	 * secondary index (2i) for each record consisting of the key converted to long to be used
	 * for the scan operation
	 *
	 * @param table The name of the table (Riak bucket)
	 * @param key The record key of the record to insert.
	 * @param values A HashMap of field/value pairs to insert in the record
	 * @return Zero on success, a non-zero error code on error
	 */
	@Override
	public Status insert(String table, String key, HashMap<String, ByteIterator> values) {
        try {

        	String fake_value = "fake value";	

            conn.sendRPC(java_erl,"insert", 
                new OtpErlangObject[] { new OtpErlangAtom(key), 
                					    new OtpErlangAtom(fake_value)});
            OtpErlangObject response = conn.receiveRPC(); 
            OtpErlangAtom at = (OtpErlangAtom) response;
            String res = at.atomValue();
            if (!res.equals("ok")) {
                return Status.ERROR;
            }
            
        }
        catch (Exception exp) {
            System.out.println("Error is :" + exp.toString());
            exp.printStackTrace();
            return Status.ERROR;
        }
        return Status.OK;
	}
	
	
	/**
	 * Update a record in the database. Any field/value pairs in the specified values 
	 * HashMap will be written into the record with the specified
	 * record key, overwriting any existing values with the same field name.
	 *
	 * @param table The name of the table (Riak bucket)
	 * @param key The record key of the record to write.
	 * @param values A HashMap of field/value pairs to update in the record
	 * @return Zero on success, a non-zero error code on error
	 */
	@Override
	public Status update(String table, String key, HashMap<String, ByteIterator> values) {
        return insert(table, key, values);
	}
		

	/**
	 * Delete a record from the database. 
	 *
	 * @param table The name of the table (Riak bucket)
	 * @param key The record key of the record to delete.
	 * @return Zero on success, a non-zero error code on error
	 */
	@Override
	public Status delete(String table, String key) {

		
        try {
	        conn.sendRPC(java_erl,"delete", new OtpErlangObject[] {new OtpErlangAtom(key)});
	        OtpErlangObject response = conn.receiveRPC(); 
	        OtpErlangAtom at = (OtpErlangAtom) response;
	        String res = at.atomValue();
	        if (!res.equals("ok")) {
	            return Status.ERROR;
            }
          }
          catch (Exception exp) {
               System.out.println("Error is :" + exp.toString());
               exp.printStackTrace();
               return Status.ERROR;
          }
        return Status.OK;
	}

	private void loadProperties()
	{	
		Properties props = getProperties();

		String peerStr = props.getProperty(DEFAULT_ERL_NODE);
        if (peerStr != null) {
            peer = peerStr;
        }

        String cookieStr = props.getProperty(DEFAULT_ERL_COOKIE);
        if (cookieStr != null) {
        	cookie = cookieStr;
        }

        String javaClientStr = props.getProperty(DEFAULT_JAVA_CLIENT);
        if (javaClientStr != null) {
        	javaClient = javaClientStr;
        }

        String java_erlStr = props.getProperty(DEFAULT_ERL_MODULE);
        if (java_erlStr != null) {
        	java_erl = java_erlStr;
        }
  //       System.out.println("peer: " + peer);
  //       System.out.println("cookie: " + cookie);
  //       System.out.println("java_erl: " + java_erl);
		// System.out.println("propeties loaded.");
	}
}
