Riak ECC Client for Yahoo! Cloud System Benchmark (YCSB)
--------------------------------------------------------

By Viet Bui (from @uclouvain.be)

The Riak ECC YCSB client is designed to work with the Yahoo! Cloud System Benchmark (YCSB) project (https://github.com/brianfrankcooper/YCSB) to support performance testing for the 2.0.X line of the Riak database.

Based on RiakDBClient by Basho. 

How to Implement the Riak ECC Client
----------------------------
The following directions will help you get started with benchmarking Riak using the YCCB project and Riak client.

* Download the YCSB project from https://github.com/brianfrankcooper/YCSB and extract the contents onto the machine, or machines, you plan to execute the project from. <b>Note</b>: YCSB requires Java and Maven.

* Download the YCSB-RiakECC-Binding project and copy the Riakecc folder into the YCSB folder.

* Modify the following sections of the YCSB's POM file to add the Riak client:

```
<properties>
  ...
  <riakecc.version>2.0.0</riakecc.version>
  ...
</properties>
```

```
<modules>
  ...
  <module>riakecc</module>
  ...
</modules>
```

* Modify the <b>ycsb</b> file in the YCSB <b>bin</b> folder to add the Riak client in the DATABASES section as shown below::
```
DATABASES = {
    "basic"        : "com.yahoo.ycsb.BasicDB",
    ...
    "riakecc"         : "com.yahoo.ycsb.db.RiakEccClient"
}
```

* Build the YCSB project by running the following command in the YCSB root

```
mvn -pl com.yahoo.ycsb:riakecc-binding -am clean package
```

* Configure parameters in /src/main/resources/riak.properties. Default is: 

```
#!java

DEFAULT_BUCKET_TYPE=ycsb
DEFAULT_ERL_NODE = erlNode@localhost;
DEFAULT_ERL_COOKIE = ecc-ycsb;
DEFAULT_JAVA_CLIENT = javaNode;
DEFAULT_ERL_MODULE = ecc_java;
```

* Run a riak node: 

```
#!shell
./riak start
```

* Run an Ecc client (with configured port that agreed with Riak), with node name and cookie agreed in riak.properties. For example: 

```
#!shell
erl -pa ./ebin ~/path-to/riak-erlang-client/ebin/ ~/part-to/riak-erlang-client/deps/*/ebin -sname erlNode@localhost -setcookie ecc-ycsb

```

* In erlang shell, start the application: 


```
#!erlang

(erlNode@localhost)2> application:start(ecc).
```


* Load and run a YCSB workload using the RiakEcc client:

```
./bin/ycsb load riakecc -P workloads/workloada > riakecc-load.txt
./bin/ycsb run riakecc -P workloads/workloada > riakecc-run.txt
```